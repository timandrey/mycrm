<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Company::class, 50)->create()->each(function($c) {
            $c->employees()->save(factory(App\Models\Employee::class)->make());
        });
    }
}
